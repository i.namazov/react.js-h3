import React, {Component} from 'react';
import './CartInform.scss'
class CartInform extends Component {
    render() {
        const imageUrl = require(`../../../public/img/${this.props.product.path}`);
        return (
            <ul>
            <li className={"cart-list-item"}>
                <div>

                <img src={imageUrl} className={"image"}  />


                    <div className="text">
                        <p className={"name"}>{this.props.product.name}</p>

                        <p className={"product-text"}>
                            Lorem ipsum dolor sit amet, con adipiscing elit, sed diam nonu.
                        </p>
                        <p className={"price"}>${this.props.product.price}</p>
                    </div>
                </div>

                <button
                    onClick={event => {
                        this.props.toggleModalWindow(event, 2, null, this.props.product);
                    }}
                >
                    X
                </button>
            </li>
        </ul>
        );
    }}
export default CartInform;