import React, {Component} from 'react';
import './Navbar.scss';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

const Navbar = () => {
    return (
        <Router>
        <header className={"navbar"}>
            <Link to={"/"}>Carts</Link>
            <Link to={"/cart"}>      Cart</Link>
            <Link to={"/favorites"}>       Favorites</Link>
        </header>
        </Router>
    );
};

export default Navbar;