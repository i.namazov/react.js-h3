import React, {Component} from 'react';
import CartInform from "../CartInform/CartInform";
import './CartMatter.scss'
class CartMatter extends Component {
    render() {
        return (
            <div>{this.props.cartInform ?
                this.props.cartInform.map((item, ind) => (
                        <CartInform
                            key={ind}
                            product={item}
                            toggleModalWindow={this.props.toggleModalWindow}
                        />
                    ))
                    : <h2 className={"Empty"}>You don't enter the cart</h2>}
            </div>
        );
    }}

export default CartMatter;